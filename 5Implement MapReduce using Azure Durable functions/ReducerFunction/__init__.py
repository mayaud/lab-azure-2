def main(wordGroup: tuple) -> dict:
    return {wordGroup[0]: sum(wordGroup[1])}
