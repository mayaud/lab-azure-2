import azure.functions as func
import azure.durable_functions as df
import logging

def orchestrator_function(context: df.DurableOrchestrationContext):
    input_data = yield context.call_activity('GetInputDataFn', {
        'connection_string': 'DefaultEndpointsProtocol=https;AccountName=inputmapreduce;AccountKey=e3TwBWifAdgDydsqdGjz22ae8XrslqMOrfbRNuRlvvHOKrhJflBkRUAWh7KH2dP1Ql9X9Ap1BVfD+AStmNuAPw==;EndpointSuffix=core.windows.net', 
        'container_name': 'test'
    })

    map_tasks = [context.call_activity("MapperFunction", line) for offset, line in input_data]
    mapper_results = yield context.task_all(map_tasks)


    shuffled_results = yield context.call_activity("ShufflerFunction", mapper_results)


    reduce_tasks = [context.call_activity("ReducerFunction", (key, value)) for key, value in shuffled_results.items()]
    reducer_results = yield context.task_all(reduce_tasks)


    return reducer_results

main = df.Orchestrator.create(orchestrator_function)