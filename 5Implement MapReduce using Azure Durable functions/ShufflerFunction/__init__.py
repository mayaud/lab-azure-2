import logging

def main(mapperResults: list) -> dict:
    shuffle_dict = {}
    for word_count_list in mapperResults:
        for word, count in word_count_list:
            shuffle_dict.setdefault(word, []).append(count)
    return shuffle_dict