from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

def main(inputData: dict) -> list:
    container_name = inputData.get('container_name')
    connection_string = inputData.get('connection_string')
    blob_service_client = BlobServiceClient.from_connection_string(connection_string)
    container_client = blob_service_client.get_container_client(container_name)
    
    input_data = []
    blob_list = container_client.list_blobs()
    for blob in blob_list:
        blob_client = container_client.get_blob_client(blob)
        stream = blob_client.download_blob()
        text = stream.readall().decode('utf-8')
        lines = text.split('\n')
        for offset, line in enumerate(lines):
            input_data.append((offset, line))
    
    return input_data