from locust import HttpUser, task, between

class UserBehavior(HttpUser):
    wait_time = between(1, 2)

    @task
    def load_test(self):
        self.client.get("/")