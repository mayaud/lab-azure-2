from flask import Flask, request
import numpy as np

app = Flask(__name__)

def f(x):
    return abs(np.sin(x))

def integrale(x_min, x_max, N):
    sum = 0
    dx = (x_max - x_min) / N
    for k in range(N):
        left = x_min + k * dx
        right = x_min + (k + 1) * dx
        sum += dx * f((left + right) / 2)
    return sum

@app.route('/numericalintegralservice', methods=['GET'])
def calculate_integral():
    x_min = float(request.args.get('x_min', 0))  
    x_max = float(request.args.get('x_max', 3.14159))  
    results = {}
    # for N in [10, 100, 1000, 10000, 100000, 1000000]:
    for N in [100000]:
        result = integrale(x_min, x_max, N)
        results[f'N={N}'] = result
    return results

if __name__ == '__main__':
    app.run(debug=True)
