import azure.functions as func
import logging
import numpy as np

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)
    
@app.route(route="numericalintegralservice")
def calculate_integral(req: func.HttpRequest):
    try:
        x_min = float(req.params.get('x_min', 0))  
        x_max = float(req.params.get('x_max', 3.14159))
    except ValueError as e:
        return func.HttpResponse(
            f"Invalid input for x_min or x_max: {e}",
            status_code=400
        )

    results = {}
    for N in [100000]:
        result = integrale(x_min, x_max, N)
        results[f'N={N}'] = result

    return func.HttpResponse(
        f"{results}",
        status_code=200
    )

def integrale(x_min, x_max, N):
    sum = 0
    dx = (x_max - x_min) / N
    for k in range(N):
        left = x_min + k * dx
        right = x_min + (k + 1) * dx
        sum += dx * f((left + right) / 2)
    return sum

def f(x):
    return abs(np.sin(x))
